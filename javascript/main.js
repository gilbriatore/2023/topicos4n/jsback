//Variáveis var, let, const e typeof string, number e boolean;
//Operadores (Math) (=, +, -, *,  /, %, <, >, >=, ==, !=, ||, &&);
//Estruturas de decisão if/else, ? : e switch;
//Estruturas de repetição while, do/while, for, for/in e for/of;
//Vetores e funções tradicional e seta.

//const algumValor = 10 == 9;
//console.log(algumValor);
//let raiz = Math.sqrt(9);
//console.log(raiz);

// 1) cenário

/**
let nota1 = 8;
let nota2 = 6;

let notaFinal = (nota1 + nota2) / 2;

if (notaFinal >= 6){
    console.log("Aprovado!");
} else {
    console.log("Reprovado!");
}
 */

// 2) cenário

// let vetor = [10, 6, 8, 9, 8];

// for(numero of vetor){
//     console.log(numero);
// }

// for(index in vetor){
//     console.log(index);
// }


// for(let i = 0; i < 5; i++){
//     const valor = vetor[i];
//     console.log(valor);
// }

// let contador = 0;
// while(contador < 5){
//     const valor = vetor[contador];
//     console.log(valor);
//     contador++;
// }


// function somar(a, b){
//     return a + b;
// }

// const subtrair = (a, b) => a - b;

// let valor = subtrair(20, 13);
// console.log(valor);


// let valor = somar(10, 35);
// console.log(valor);


// Módulos
// const calc = require('./calculadora');

// console.log( calc.somar(10, 5) );
// console.log( calc.subtrair(10, 5) );
// console.log( calc.multiplicar(10, 5) );
// console.log( calc.dividir(10, 5) );

const prompt = require("prompt-sync")();

let valor = prompt("Informe um valor");
console.log(valor);